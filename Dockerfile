# Use the official Debian image as a parent image
FROM debian:bullseye-slim

# Install Python 3.9
RUN apt-get update && apt-get install -y python3.9 python3-pip

# Clean the apt cache before running the update and install commands
RUN rm -rf /var/lib/apt/lists/*

# Install Docker CLI
RUN apt-get update && apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common \
    lsb-release \
    && rm -rf /var/lib/apt/lists/*

# Add Docker’s official GPG key
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -

# Add Docker repository
RUN echo "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list

# Install Docker CLI
RUN apt-get update && apt-get install -y docker-ce-cli && rm -rf /var/lib/apt/lists/*

# Install Heroku CLI
RUN curl https://cli-assets.heroku.com/install.sh | sh

# Upgrade pip
RUN python3.9 -m pip install --upgrade pip

# Install required Python packages
COPY requirements.txt /app/requirements.txt
RUN python3.9 -m pip install --no-cache-dir -r /app/requirements.txt

# Download necessary NLTK data
RUN python3.9 -m nltk.downloader stopwords punkt wordnet

# Set the working directory
WORKDIR /app

# Copy the application files
COPY . .

# Expose the port that Heroku will use
EXPOSE $PORT

# Set environment variables for Streamlit
ENV STREAMLIT_SERVER_HEADLESS=true

# Command to run the application
CMD ["sh", "-c", "streamlit run src/app.py --server.port=$PORT --server.enableCORS=false"]
