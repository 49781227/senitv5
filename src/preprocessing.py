import pickle as pk
import pandas as pd
from sklearn.model_selection import train_test_split
from nltk.stem import SnowballStemmer
from nltk.corpus import stopwords
import re
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
import spacy
import re


my_data = pd.read_pickle('/builds/49781227/senitv5/data/processed/my_data.pkl')
replace_data = {0: -1, 4: 1}
my_data.replace(replace_data, inplace=True)


delete_pattern = "@\S+|https?:\S+|http?:\S|[^A-Za-z0-9]+"
stopwords = stopwords.words('english')
stemmer = SnowballStemmer("english")

def preprocess_text_new(text):
    # Remove link and special characters
    text = re.sub(delete_pattern, ' ', text.lower()).strip()
    tokens = word_tokenize(text)
    # Remove stop words
    tokens = [token for token in tokens if token not in stopwords]
    # Stemming the tokens
    tokens = [stemmer.stem(token) for token in tokens]
    return " ".join(tokens)

my_data['preprocessed_text'] =  my_data['text'].apply(preprocess_text_new)
my_data.drop('text', axis=1, inplace = True)
my_data.columns = ['target', 'processed_text']
my_data.to_pickle('/builds/49781227/senitv5/data/processed/my_data_processed.pickle')

