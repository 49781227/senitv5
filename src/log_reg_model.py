
import pickle as pk
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
import preprocessing
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import ConfusionMatrixDisplay
from sklearn.metrics import classification_report


# Load DataFrame from pickle file
my_data_processed = pd.read_pickle('/builds/49781227/senitv5/data/processed/my_data_processed.pickle')

txt = my_data_processed["processed_text"]
targett = my_data_processed.target

txt_train, txt_test, targett_train, targett_test = train_test_split(txt, targett, test_size=0.2, random_state=42)
print(txt_train.shape, txt_test.shape, targett_train.shape, targett_test.shape)

# Load the vectorizer
vectorizer = TfidfVectorizer(max_features=2500)

# Save the vectorizer
with open('/builds/49781227/senitv5/models/vectorizer.pkl', 'wb') as f:
    pk.dump(vectorizer, f)
txt_train_prep = vectorizer.fit_transform(txt_train)
txt_test_prep = vectorizer.transform(txt_test)

log_reg = LogisticRegression(max_iter=1000)
log_reg.fit(txt_train_prep, targett_train)

#print('Train score: ', log_reg.score(txt_train_prep, targett_train))
#print('Test score: ', log_reg.score(txt_test_prep, targett_test))


# Confusion matrix
targett_pred = log_reg.predict(txt_test_prep)

#print('Classification report:')
#print(classification_report(targett_test, targett_pred))

#print('Confusion matrix:')
#ConfusionMatrixDisplay.from_predictions(targett_test, targett_pred, cmap='Blues', normalize='true');

pk.dump(log_reg,open('/builds/49781227/senitv5/models/log_reg.pkl','wb'))






