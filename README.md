# senitv5
# Tweet Sentiment Analysis

This project is a Tweet Sentiment Analysis application built with Streamlit. It allows users to enter tweets and predict their sentiment (positive or negative) using a pre-trained machine learning model.

# Diagram


                       +-------------+
                       |  GitLab CI  |
                       |    Pipelines|
                       +------+------+
                              |
                              v
+------------------+    +-----+-------+      +-------------+
|  Streamlit App   +--->| Docker Image |---->|  Heroku     |
|  (src/app.py)    |    | (Container)  |     |  Deployment |
+------------------+    +-------------+      +-------------+
        ^                     ^
        |                     |
        |                     |
+-------+-------+     +-------+------+
|   Scikit-learn |    |    NLTK      |
|  (ML Model)    |    | (Text Processing)|
+---------------+     +---------------+


## Project Structure

senitv5/
│
├── src/
│ ├── app.py
│ ├── log_reg_model.py
│ ├── make_dataset.py
│ └── preprocessing.py
├── models/
│ ├── log_reg.pkl
│ └── vectorizer.pkl
├── data/
│ ├── raw/
│ │ └── data.csv
│ ├── processed/
│ │ ├── my_data.pkl
│ │ └── my_data_processed.pkl
│ ├── preprocessed/
│ └── other_data_files
├── requirements.txt
├── Dockerfile
├── Procfile
├── .gitlab-ci.yml
└── README.md


## Prerequisites

- Python 3.9
- Docker
- GitLab account
- Heroku account


## Create and Activate a Virtual Environment

python3.9 -m venv venv
source venv/bin/activate

## Install Dependencies

pip install -r requirements.txt

## Train the Model
If you need to train the model, you can run the training script:

python src/log_reg_model.py


## Run the Application Locally
streamlit run src/app.py

## Deployment
This project uses GitLab CI/CD for continuous integration and deployment to Heroku.

Set Up GitLab CI/CD
Ensure the following environment variables are set in GitLab project settings (Settings > CI / CD > Variables):

CI_REGISTRY_USER: gitlab-ci-token
CI_REGISTRY_PASSWORD: Your GitLab personal access token or the predefined CI_JOB_TOKEN
HEROKU_API_KEY: Your Heroku API key
PIP_PATH: requirements.txt
STREAMLIT_PORT: 8501
IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
DOCKER_HOST: tcp://docker:2375/
DOCKER_TLS_CERTDIR: ""
Dockerfile
Ensure Dockerfile is correctly set up to build the image and run the Streamlit app.

## Procfile

Ensure you have a Procfile in your project root with the following content:
web: streamlit run src/app.py --server.port $PORT --server.enableCORS false

## Commit and Push Changes
Commit the changes to the repository to trigger the CI/CD pipeline:
git add .
git commit -m "Set up CI/CD pipeline"
git push origin main

## Monitor the Pipeline
Go to GitLab project and monitor the pipeline under CI / CD > Pipelines. Ensure all stages complete successfully.

## Access Application
Once deployed, you can access application at https://sentiment5.herokuapp.com.

## Usage
Open the application in web browser.
Enter a tweet in the textbox.
Click the "Predict Sentiment" button.
The application will display whether the sentiment is positive or negative.


## Acknowledgments
Streamlit for the awesome framework.
Heroku for the easy-to-use deployment platform.
GitLab for the powerful CI/CD tools.


